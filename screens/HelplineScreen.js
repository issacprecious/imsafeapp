import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HelplineScreen = props => {
    return(
        <View style={styles.screen}>
            <Text>This is the NGO Helpline Screen.</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
})

export default HelplineScreen;