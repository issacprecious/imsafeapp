import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, ImageBackground, Button, TouchableOpacity, StatusBar, StatusBarIOS } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

const WelcomeScreen = props => {

    const dispatch = useDispatch();

    // useEffect(() => {
    //     console.log('dispatch...,'useStore);
    // })

    const currentState = useSelector((state) => state);
    console.log('current State..test',currentState);
    
    return(
        <View style={styles.screen}>
            <View>
                <View style={styles.baseView}>
                    <Image source={require('../assets/girls.png')}>
                    </Image>
                </View>
                <View style={styles.layerView}>
                    <Image source={require('../assets/welcomePath.png')}>
                    </Image>
                </View>
                <View style={styles.textLayer}>
                    <Text style={styles.headingText}>Hi! Welcome to I'm Safe.</Text>
                    <Text style={styles.subHeadingText}>Let us guide you to an easy 4 step registration process that will enable this app to act as your own security guard.</Text>
                    <View style={styles.featureContainer}>
                        <Image source={require('../assets/sirenIcon.png')} style={styles.contentImage}></Image>
                        <Text style={styles.contentText}>Click button in app during emergencies</Text>
                    </View>
                    <View style={styles.featureContainer}>
                        <Image source={require('../assets/mailIcon.png')} style={styles.contentImage}></Image>
                        <Text style={styles.contentText}>Automatically send out SMS to trusted contacts and helpline</Text>
                    </View>
                    <View style={styles.featureContainer}>
                        <Image source={require('../assets/updatesIcon.png')} style={styles.contentImage}></Image>
                        <Text style={styles.contentText}>Stay updated with the latest safety tips, resources and insights</Text>
                    </View>
                    <View style={styles.featureContainer}>
                        <Image source={require('../assets/imSafeIcon.png')} style={styles.contentImage}></Image>
                        <Text style={styles.contentText}>Share your thoughts, experiences and support your community</Text>
                    </View>
                    <View style={styles.submitContainer}>
                        <TouchableOpacity
                            style={styles.submitButtonStyle}
                            activeOpacity = { .5 }
                            onPress={() => props.navigation.navigate({routeName: 'CompleteProfile'})}>
                                <Text style={styles.buttonTextStyle}> Let's Go </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View> 
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#454AD3"
    },
    baseView:{
        flex:1,
        //height: Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        alignItems: "flex-start",
    },
    layerView:{
        flex:1,
        width:Dimensions.get('window').width,
        alignItems: "flex-start",
        marginTop: (Platform.OS === 'ios') ? 0 : -50,
        position: 'absolute',
    },
    textLayer: {
        flex:1,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        position: 'absolute',
        justifyContent: "flex-start",
        alignItems: "flex-start",
    },
    headingText: {
        color: "#CBCDFF",
        fontFamily: 'Poppins-Bold',
        fontSize: 27,
        paddingTop: (Platform.OS === 'ios') ? '75%' : '60%',
        paddingLeft: 20,
    },
    subHeadingText: {
        color: "#CBCDFF",
        fontFamily: 'Poppins-Bold',
        fontSize: 15,
        paddingTop: "2%",
        padding: 20
    },
    contentText: {
        flex:1,
        color: "#CBCDFF",
        fontFamily: 'Poppins-Regular',
        fontSize: (Platform.OS === 'ios') ? 16 : 12,
        alignItems: "center",
        width: Dimensions.get('window').width,
        paddingRight: 20,
    },
    contentImage: {
        margin: 20,
    },
    featureContainer: {
        flex:1,
        flexDirection: "row",
        display: "flex",
        alignItems: "center",
    },
    submitContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: Dimensions.get('window').width,
    },
    submitButtonStyle: {
        width: "40%",
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        backgroundColor:'#DE3584',
        borderRadius:50,
        marginBottom: 30,
      },
      buttonTextStyle:{
          color:'#fff',
          textAlign:'center',
      }
})

export default WelcomeScreen;
