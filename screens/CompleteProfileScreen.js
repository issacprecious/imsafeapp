import React, { Component, Fragment } from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView, Button } from 'react-native';
import BlurOverlay, {closeOverlay, openOverlay} from 'react-native-blur-overlay';
import {connect} from 'react-redux';
import {Formik} from 'formik';
import * as yup from 'yup';
const schema = yup.object({
   
    mobileNumber: yup.string().required()
    
   
  });

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: "flex-end"
    },
    headingContainer: {
        alignItems: "flex-start",
        justifyContent: "space-evenly",
        paddingLeft: '10%',
        //backgroundColor: 'yellow',
        //flex: 1,
        minHeight: '30%'
    },
    headingText: {
        marginTop: (Platform.OS === 'ios') ? 100 : 50,
        color: "#DE3584",
        fontSize: 30,
        fontFamily: 'Poppins-Bold',
    },
    subHeadingText: {
        paddingRight: '10%',
        paddingBottom: '2%',
        color: "#E86DA6",
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
    },
    inputFieldsContainer: {
        alignItems: "flex-start",
        paddingLeft: '10%',
        //backgroundColor: 'orange',
        justifyContent: "space-around",
        minHeight: '50%'
    },
    userNameContainer: {
        //backgroundColor: 'purple',
        width: Dimensions.get('window').width,
        paddingBottom: '2%',
    },
    textInputHeading: {
        color: "#BCBCBC",
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
    },
    textInputHeadingLeft: {
        alignItems: "flex-start",
        color: "#BCBCBC",
        flex: 1,
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
    },
    textInputHeadingRight: {
        alignItems: "flex-end",
        paddingRight: '12%',
        color: "#454AD3",
        fontSize: 12,
        flex: 1,
        fontFamily: 'Poppins-Regular',
    },
    emailInputHeadingRight: {
        alignItems: "flex-end",
        paddingRight: '12%',
        color: "#E86DA6",
        fontSize: 12,
        flex: 2,
        fontFamily: 'Poppins-Regular',
    },
    textInput: {
        textAlign: "left",
        paddingHorizontal: 20,
        height: 50,
        borderWidth: 1,
        borderColor: '#DE3584',
        borderRadius: 50 ,
        backgroundColor : "#FFFFFF",
        width: '80%'
    },
    doubleTextInput: {
        textAlign: "left",
        paddingHorizontal: 20,
        height: 50,
        borderWidth: 1,
        borderColor: '#DE3584',
        borderRadius: 50 ,
        backgroundColor : "#FFFFFF",
        width: '43%',
    },
    phoneNumberContainer: {
        //backgroundColor: 'green',
        width: Dimensions.get('window').width,
        paddingBottom: '2%',
    },
    doubleHeadingContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    doubleInputContainer: {
        //backgroundColor: 'gray'
        paddingBottom: '2%'
    },
    doubleInputHeadingContainer: {
        //backgroundColor: 'yellow',
        flexDirection: "row",
        justifyContent: "flex-start"
    },
    /* */
    submitContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        //backgroundColor: 'purple',
        width: Dimensions.get('window').width,
        minHeight: '20%'
    },
    submitButtonStyle: {
        width: "40%",
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        backgroundColor:'#454AD3',
        borderRadius:50,
        marginBottom: 30,
      },
      buttonTextStyle:{
        color:'#fff',
        fontSize: 15,
        textAlign:'center',
    },/* */
    myLeftPadding: {
        marginLeft: '3%'
    },
    popupContainer: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#454AD3',
        width: '90%',
        borderRadius:25,
        paddingBottom: '5%'
    },
    popupHeadingText: {
        alignItems: "center",
        color: "#CBCDFF",
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        paddingTop: '10%'
    },
    popupSubHeadingText: {
        alignItems: "center",
        color: "#CBCDFF",
        fontSize: 22,
        fontFamily: 'Poppins-Bold',
    },
    popupHintText: {
        alignItems: "center",
        color: "#BCBCBC",
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        paddingTop: '15%',
    },
    otpTextInputContainer: {
        flexDirection: "row",
        justifyContent: "center",
    },
    otpTextContainer: {
        flexDirection: "row",
        justifyContent: "center",
        paddingTop: '10%'
    },
    otpTextInput: {
        textAlign: "center",
        //paddingHorizontal: 20,
        height: 50,
        borderBottomColor: '#FFFFFF',
        borderBottomWidth: 1,
        width: '10%',
        marginLeft: 5,
        marginRight: 5,
        color: "#A0A0A0",
        fontSize: 25,
        fontFamily: 'Poppins-Bold',
    },
    resendContainer: {
        flexDirection: "row",
        minWidth: '40%',
    },
    endsInContainer: {
        flexDirection: "row",
        minWidth: '40%',
        //backgroundColor: 'red',
        justifyContent: "flex-end"
    },
    resendText: {
        color: "#BCBCBC",
        fontSize: 15,
        paddingHorizontal: 10,
    },
    endsInText: {
        color: "#BCBCBC",
        fontSize: 15,
        alignContent: "flex-end",
    },
    timerText: {
        color: "#BCBCBC",
        paddingLeft: 10,
        fontSize: 15,
        color: 'white',
        
    },
    /* */
    popupSubmitContainer: {
        alignItems: "center",
        justifyContent: "center",
        width: Dimensions.get('window').width,
        paddingTop: '10%'
    },
    popupSubmitButtonStyle: {
        width: "40%",
        paddingTop:15,
        paddingBottom:15,
        backgroundColor:'#DE3584',
        borderRadius:50,
    },
    popupButtonTextStyle:{
        color:'#fff',
        textAlign:'center',
    }
    /* */
})


export class CompleteProfileScreen extends Component {
    constructor(props){       
        super(props);   
        this.state ={
          username:(props.user.displayName!='') ? props.user.displayName : '',     
          mobileNumber:(props.user.mobileNumber!='') ? props.user.mobileNumber : '',
          dateOfBirth:(props.user.dateOfBirth!='') ? props.user.dateOfBirth : '',
          facebookId:(props.user.facebookId!='') ? props.user.facebookId : '',
          sosSecret:(props.user.sosSecret!='') ? props.user.sosSecret : '',
          confirmSosSecret:(props.user.confirmSosSecret!='') ? props.user.confirmSosSecret : '',
          email:(props.user.email!='') ? props.user.email : '',
          gender:(props.user.gender!='') ? props.user.gender : ''       
          
      };
      this.onSubmit = this.onSubmit.bind(this);
      //console.log('TEasdfasd')
     // this.onSubmit = this.onSubmit.bind(this);
      }
      onSubmit(values , { setSubmitting }) {        
        setSubmitting(false);  
        alert(values)      
}
    renderBlurChilds = () => {
        return (
            <View style={styles.popupContainer}>
                
                <Text style={styles.popupHeadingText}>We just sent to you a code.</Text>
                <Text style={styles.popupSubHeadingText}>Usually arrives fast…</Text>
                <Text style={styles.popupHintText}>Insert the 6-digit code</Text>
                <View style={styles.otpTextInputContainer}>
                    <TextInput style={styles.otpTextInput}></TextInput>
                    <TextInput style={styles.otpTextInput}></TextInput>
                    <TextInput style={styles.otpTextInput}></TextInput>
                    <TextInput style={styles.otpTextInput}></TextInput>
                    <TextInput style={styles.otpTextInput}></TextInput>
                    <TextInput style={styles.otpTextInput}></TextInput>
                </View>
                <View style={styles.otpTextContainer}>
                    <View style={styles.resendContainer}>
                        <Image source={require('../assets/refresh.png')}></Image>
                        <Text style={styles.resendText}>Resend</Text>
                    </View>
                    <View style={styles.endsInContainer}>
                        <Text style={styles.endsInText}>It’s ends in</Text>
                        <Text style={styles.timerText}>03 : 59</Text>
                    </View>
                </View>
                <View style={styles.popupSubmitContainer}>
                    <TouchableOpacity
                        style={styles.popupSubmitButtonStyle}
                        activeOpacity = { .5 }
                        //onPress={() => { closeOverlay(); }}>
                        onPress={() => props.navigation.navigate({routeName: 'GetPermissions'})}>
                            <Text style={styles.popupButtonTextStyle}> Verify </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.screen} keyboardVerticalOffset={Platform.select({ios: 0, android: -500})} behavior="padding">
           <ScrollView>    
           <Formik
    initialValues={{ email: '' }}
    onSubmit={this.onSubmit}
    validationSchema={schema}
  >
    {({ handleChange, handleBlur, handleSubmit, values,errors }) => (
      <Fragment>           
      <View style={styles.headingContainer}>
          <Text style={styles.headingText}>Complete Profile</Text>
          <Text style={styles.subHeadingText}>Let us guide you to an easy 4 step registration process that will enable this app to act as your own security guard.</Text>
      </View>
      
      <View style={styles.inputFieldsContainer}>
          <View style={styles.userNameContainer}>
              <Text style={styles.textInputHeading}>User Name</Text>
              <TextInput style={styles.textInput} value={this.state.username}></TextInput>
          </View>
          <View style={styles.phoneNumberContainer}>
              <View style={styles.doubleHeadingContainer}>
                  <Text style={styles.textInputHeadingLeft} onBlur={handleBlur('mobileNumber')} onChangeText={handleChange('mobileNumber')}>Mobile Number</Text>
                  <Text style={styles.textInputHeadingRight}>{errors.mobileNumber}</Text>
              </View>
              <TextInput style={styles.textInput} value={this.state.mobileNumber}></TextInput>
          </View>
          <View style={styles.phoneNumberContainer}>
              <View style={styles.doubleHeadingContainer}>
                  <Text style={styles.textInputHeadingLeft}>Email</Text>
                  <Text style={styles.emailInputHeadingRight}>Never Spammed, Never Shared</Text>
              </View>
              <TextInput style={styles.textInput} value={this.state.email}></TextInput>
          </View>
          <View style={styles.doubleInputContainer}>
              <View style={styles.doubleInputHeadingContainer}>
                  <Text style={styles.textInputHeadingLeft}>Gender</Text>
                  <Text style={styles.textInputHeadingLeft}>Year of Birth</Text>
              </View>
              <View style={styles.doubleInputHeadingContainer}>
                  <TextInput style={styles.doubleTextInput} value={this.state.gender}></TextInput>
                  <TextInput style={{...styles.doubleTextInput, ...styles.myLeftPadding}} value={this.state.dateOfBirth}></TextInput>
              </View>
          </View>
          <Button onPress={handleSubmit} title="Submit" />
      </View>

      <View style={styles.submitContainer}>
          <TouchableOpacity
              style={styles.submitButtonStyle}
              activeOpacity = { .5 }
              onPress={() => openOverlay()}>
                  <Text style={styles.buttonTextStyle}> Next </Text>
          </TouchableOpacity>          
      </View>
      
      </Fragment> 
     
    )}
  </Formik>
            
  </ScrollView>
            <BlurOverlay
                radius={5}
                downsampling={0}
                brightness={10}
                //onPress={() => { closeOverlay(); }}
                customStyles={{alignItems: 'center', justifyContent: 'center'}}
                blurStyle="light"
                children={this.renderBlurChilds()}
            />

        </KeyboardAvoidingView>
        )
    }
}
const mapStateToProps = (state) =>{
    console.log(state);
    return {
        user:state.auth.user
    }
  }
export default connect(mapStateToProps) (CompleteProfileScreen);
