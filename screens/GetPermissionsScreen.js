import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';

const GetPermissionsScreen = props => {
    return(
        <View style={styles.screen}>
            <Image style={styles.logoStyles} resizeMode="contain" source={require('../assets/logo.png')}></Image>
            <Text style={styles.headingStyles}>This app requires the following permissions to work as intended:</Text>
            <View style={styles.allPermissionContainer}>
                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/photoCamera.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>Camera</Text>
                        <Text style={styles.permissionDescription}>Take pictures and videos during emergency</Text>
                    </View>
                </View>
                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/microphone.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>Microphone</Text>
                        <Text style={styles.permissionDescription}>Allow audio to be recorded</Text>
                    </View>
                </View>
        
                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/data.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>Data Connection</Text>
                        <Text style={styles.permissionDescription}>Switch on Mobile Data</Text>
                    </View>
                </View>
                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/location.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>Location</Text>
                        <Text style={styles.permissionDescription}>Turn On GPS</Text>
                    </View>
                </View>

                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/phoneBook.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>Contact</Text>
                        <Text style={styles.permissionDescription}>Access contacts</Text>
                    </View>
                </View>

                <View style={styles.permissionContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../assets/envelope.png')}/>
                    </View>
                    <View style={styles.innerTextContainer}>
                        <Text style={styles.permissionHeading}>SMS</Text>
                        <Text style={styles.permissionDescription}>Send Message</Text>
                    </View>
                </View>
            </View>

            <View style={styles.submitContainer}>
                <TouchableOpacity
                    style={styles.submitButtonStyle}
                    activeOpacity = { .5 }
                    onPress={() => props.navigation.navigate({routeName: 'Home'})}>
                        <Text style={styles.buttonTextStyle}> Accept </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "space-evenly",
        alignItems: "center",
        backgroundColor: "#454AD3",
        paddingHorizontal: 35
    },
    logoStyles: {
        marginTop: 20,
        height: '10%'
    },
    headingStyles: {
        color: "#CBCDFF",
        fontSize: 25,
        fontFamily: 'Poppins-Bold',
    },
    innerTextContainer: {
        justifyContent: "center",
    },    
    iconContainer: {
        justifyContent: "center",
        width: "15%",
        //backgroundColor: "black",
        alignItems: "center"
    },
    allPermissionContainer: {
        justifyContent: "space-evenly",
        height: '55%'
    },
    permissionContainer: {
        flexDirection: "row",
        //backgroundColor: 'yellow',
        width: '100%',
        padding: 5,
    },
    permissionHeading: {
        color: "#CBCDFF",
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
    },
    permissionDescription: {
        color: "#CBCDFF",
        fontFamily: 'Poppins-Regular',
        fontSize: 12,
    },
    submitContainer: {
        width: '100%',
        //justifyContent: "center",
        alignItems: "center",
        //backgroundColor: 'black'
    },
    submitButtonStyle: {
        width: "40%",
        backgroundColor:'#DE3584',
        borderRadius: 50,
        paddingTop:15,
        paddingBottom:15,
    },
    buttonTextStyle:{
        color:'#fff',
        fontSize: 15,
        textAlign:'center',
    }
})

export default GetPermissionsScreen;