import React from 'react';
import { View, Text, StyleSheet, Button, Image, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { LoginManager,LoginButton,AccessToken } from "react-native-fbsdk";
import { useDispatch, connect } from 'react-redux';
import { userActions } from '../store/actions';

const LoginScreen = props => {

  const dispatch = useDispatch();

  const onFacebookLogin = () => {
    LoginManager.logInWithPermissions(["public_profile","email"]).then(
      function(result) {
        //console.log('test result...',result);
        if (result.isCancelled) {
         console.log("Login cancelled");
        } else {
          
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const fbToken = data.accessToken.toString();
              dispatch(userActions.facebookLogin(fbToken));
              if(props.authUser.token){
              props.navigation.navigate({routeName: 'CompleteProfile'})
              }
              //console.log('auth:',props.authUser)              
            }
          )

        }
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );
  }
  
    return(
      <View style={styles.screenBackground}>
      <ImageBackground 
          source={require('../assets/logoLine.png')}
          style={styles.backgroundImage}
            imageStyle={{
              alignSelf: "flex-start",
              resizeMode: 'contain' // works only here!
            }}
        >
        <View style={styles.screen}>
          <Image source={require('../assets/imSafeLogo.png')} resizeMethod='auto' />
          <Text style={styles.loginText}>Login</Text>

          {/* () => props.navigation.navigate({routeName: 'Welcome'}) */}
          <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5} onPress={onFacebookLogin}>
            <Image
              source={require('../assets/fbButton.png')}
              style={styles.ImageIconStyle}
            />
          </TouchableOpacity>
          <View style={styles.bottomView}>
            <Text style={styles.disclaimerText}>
              By tapping Continue, Create account, I agree to Company's Terms of Service, Payments Terms of Service, Privacy Policy, and Non-discrimination Policy.
            </Text>
          </View>
            {/* Working Facebook Login Code * /      
              <LoginButton
            onLoginFinished={
              (error, result) => {
                if (error) {
                  console.log("login has error: " + result.error);
                } else if (result.isCancelled) {
                  console.log("login is cancelled.");
                } else {
                  AccessToken.getCurrentAccessToken().then(
                    (data) => {
                      console.log(data.accessToken.toString());
                      //props.navigation.navigate({routeName: 'Welcome'})
                    }
                  )
                }
              }
            }
            onLogoutFinished={() => console.log("logout.")}/>
              /* */}
          </View>
          </ImageBackground>
          </View>
    );
};

const styles = StyleSheet.create({ 
  backgroundImage: { 
    flex: 1,
    width: 200,
    height: null,
    paddingLeft: -100,
    position: "relative",
    },
    screen: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      width: Dimensions.get('window').width
    },
    screenBackground: {
      flex: 1,
      backgroundColor: '#DE3584'
    },
    loginText: {
      paddingTop: '20%',
      paddingBottom: '10%',
      fontFamily: 'Poppins-Bold',
      fontSize: 30,
      color: '#FF50A2',
      opacity: 1
    },
    bottomView: {
      flex:1,
      position: 'absolute',
      bottom: 0
    },
    disclaimerText: {
      textAlign: 'center',
      fontFamily: 'Poppins-Regular',
      color: '#FFFFFF',
      opacity: 1,
      fontSize:10,
      marginBottom:20
    }
})
const mapStateToProps = (state) =>{
  return {
      authUser:state.auth
  }
}
export default connect(mapStateToProps) (LoginScreen);
