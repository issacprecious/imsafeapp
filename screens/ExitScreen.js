import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const ExitScreen = props => {
    return(
        <View style={styles.screen}>
            <Text>This is the Exit Screen</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
})

export default ExitScreen;