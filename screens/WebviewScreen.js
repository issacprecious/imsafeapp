import React from 'react';
import { View, Text, StyleSheet, WebView } from 'react-native';

const WebviewScreen = props => {
    return(
        <View style={styles.screen}>
            <WebView />
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
})

export default WebviewScreen;