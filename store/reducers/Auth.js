import { userConstants } from '../constants';
const initialState = {
    name:'Test',
    email:'test@admin.com'
}

export default(state = initialState, action)=> {
  //console.log('Action: ',action);
  switch (action.type) {
    case userConstants.FACEBOOK_LOGIN_REQUEST:
        return {          
          loggingIn: true,
          token: action.response.token,
          user:action.response
        };
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}

