import {LOGIN_DETAILS} from '../actions/loginAction';
const initial_state = {
    name:'Test',
    email:'babu@ruahtech.com.au'
}


const LoginReducer = (state = initial_state,action) => {
    console.log('reducer..called');
    switch(action.type){
        case 'LOGIN_DETAILS':
            const loginDetails = action.loginCredentials;
            return {
                ...state,
                name:loginDetails,
                email:loginDetails
            };
        default:
        return state;
    }
    
}

export default LoginReducer;