import React from 'react';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'react-native-vector-icons/FontAwesome'

import WelcomeScreen from '../screens/WelcomeScreen';
import ExitScreen from '../screens/ExitScreen';
import LoginScreen from '../screens/LoginScreen';
import CompleteProfileScreen from '../screens/CompleteProfileScreen';
import GetPermissionsScreen from '../screens/GetPermissionsScreen';
import HomeScreen from '../screens/HomeScreen';
import NearMeScreen from '../screens/NearMeScreen';
import SosScreen from '../screens/SosScreen';
import InsightsScreen from '../screens/InsightsScreen';
import HelplineScreen from '../screens/HelplineScreen';

const ImSafeNavigator = createStackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            title: "",
            headerTransparent: true ,
        },
    },
    Welcome: {
        screen: WelcomeScreen,
        navigationOptions: {
            title: "",
            headerTransparent: true,
        },
    },
    CompleteProfile: {
        screen: CompleteProfileScreen,
        navigationOptions: {
            title: "",
            headerTransparent: true
        },
    },
    Exit: {
        screen: ExitScreen,
        navigationOptions: {
            title: "",
            headerTransparent: true 
        },
    },
    GetPermissions: {
        screen: GetPermissionsScreen,
        navigationOptions: {
            title: "",
            headerTransparent: true 
        },
    },
    Dashboard: {
        screen: createBottomTabNavigator({
            Home: {
                screen: HomeScreen,
                navigationOptions: {
                    tabBarLabel: 'Home',
                  },
            },
            NearMe: {
                screen: NearMeScreen,
            },
            SosScreen: {
                screen: SosScreen,
            },
            Insights: {
                screen: InsightsScreen,
            },
            Helpline: {
                screen: HelplineScreen,
            }
        }),
        navigationOptions: {
            title: "",
            headerTransparent: true,
            headerLeft: null,
        },
    },    
});

const ImSafeTabNavigator = createBottomTabNavigator({
    Login: {
        screen: ImSafeNavigator,
        navigationOptions:()=>{
            return {
              tabBarVisible:false,
            };
         }
    },
    Home: {
        screen: HomeScreen,
        navigationOptions:()=>{
            return {
              tabBarVisible:true,
            };
         }
    },
    NearMe: {
        screen: NearMeScreen,
    },
    SosScreen: {
        screen: SosScreen,
    },
    Insights: {
        screen: InsightsScreen,
    },
    Helpline: {
        screen: HelplineScreen,
    }

});

export default createAppContainer(ImSafeNavigator);