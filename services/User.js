import * as http from '../http/http';
//import axios from 'axios';

export const userService = {
    facebookLogin,
    login,
    logout,
    /*register,
    getAll,
    getById,
    update,
    delete: _delete*/
};


async function login(userData) {
    const {username, password}= userData;
    const config ={
        headers:{
            'Content-Type':'application/json'
        }
    }
    try {
        
        const auth=await axios.post(`/api/auth`,{"email":username,password},config);
        localStorage.setItem("user", auth);
        return auth;
        
    } catch (error) {
              
    }
    
}

async function facebookLogin(fbAccessToken) {
    const config ={
        headers:{
            'Content-Type':'application/json'
        }
    }
    const auth = await http.post('http://192.168.0.204:3000/api/app/user/login',{'access_token':fbAccessToken});
    //console.log('Auth :',auth)
    return auth;
  /* await http.post('http://192.168.0.204:3000/api/app/user/login',{'access_token':fbAccessToken}).then(async response=>{
  console.log('Auth :',response)
  let auth={response:response}
  console.log('Auth :',auth)
   return auth;
  }).catch(error=>{
      console.log('Server Error..', error);      
    }); */
    
//console.log('Auth:',auth)
        
          /* await axios.post(`/api/app/user/login`,{"access_token":fbAccessToken},config).then(response=>{
            console.log('http://192.168.0.204:3000/api/app/user/login', response);
            return response;
          }).catch(error=>{console.log('Server Error..', error)}); */
        // return fbAccessToken;
     // console.log(response)
        
        
    
    
}
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

/*function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`http://localhost:5000/api/auth`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`http://localhost:5000/api/auth`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`http://localhost:5000/api/auth`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`http://localhost:5000/api/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`http://localhost:5000/api/auth`, requestOptions).then(handleResponse);
}*/

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}