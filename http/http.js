
export const post = async function postData(url = '',  data = {}) {
  console.log('Form URL:',url);
  console.log('Form data:',data);
    // Default options are marked with *
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached     
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });    
    let result= await response.json(); 
 // console.log('Header:',response.headers.get('x-auth-token')) 
    result ={...result,token:response.headers.get('x-auth-token')} 
    //console.log(reponse.headers.get('x-auth-token'))
    return await result; // parses JSON response into native JavaScript objects
  }
